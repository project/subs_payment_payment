<?php

/**
 * Embeddable subs form payment
 *
 * @param $subs_types
 *   An array of subs type entities as third argument,
 *   keyed by subs type id.
 * @param $pmids (optional)
 *   The PMIDs of the payment methods the user is allowed to choose from.
 * @param array $parents
 *   An array with the machine names of the form's parent elements.
 *   @see payment_form_embedded()
 *
 * @return
 *   A form array containing basic payment elements and form handlers attached
 */
function subs_payment_payment_form($form, &$form_state, $subs_types, $pmids = array(), array $parents = array()) {

  //Store subs types selected
  $form_state['subs_types_selected'] = $subs_types;

  //Try to retrieve previous payment 
  if (!empty($form_state['payment'])) {
    $payment = $form_state['payment'];
  }
  //Otherwise, initialize it
  else {

    $currency_code = variable_get('subs_payment_payment_currency_code', FALSE);

    //Retrieve subs types names
    $subs_types_names = '';
    foreach($subs_types as $subs_type) {
      $subs_types_names[] = $subs_type->label;
    }

    //If only one item, used currency code of subs type
    //Otherwise, used default currency code
    $payment = new Payment(array(
      'currency_code' => $currency_code,
      'finish_callback' => 'subs_payment_payment_finish',
      'description' => substr(t('Subscription types @subs_types_names', array('@subs_types_names' => implode(' - ', $subs_types_names))), 0, 255),
    ));

    //Add line items for each subscriptions selected
    foreach ($subs_types as $subs_type) {
      $line_item_data = subs_payment_payment_get_line_item_data($subs_type);
      $payment->setLineItem(new PaymentLineItem($line_item_data));
    }
  }

  //Load generic payment form and add it as a children element
  $form_infos = payment_form_embedded($form_state, $payment, $pmids, $parents);

  //Merge payment form
  $form += $form_infos['elements'];

  //Display terms and conditions if available
  $terms_conditions = variable_get('subs_payment_payment_default_terms_conditions', FALSE);
  if (!empty($terms_conditions['value'])) {

    $form['terms_conditions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Terms and conditions'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['terms_conditions']['description'] = array(
      '#type' => 'markup',
      '#markup' => $terms_conditions['value'],
      '#prefix' => '<div class="subs-form-terms-conditions">',
      '#suffix' => '</div>',
    );

    $form['terms_conditions']['i_agree'] = array(
      '#type' => 'checkbox',
      '#title' => t('I agree'),
      '#required' => TRUE,
      '#attributes' => array('class' => array('subs-form-i-agree')),
    );
  }

  $form['pay'] = array(
    '#type' => 'submit',
    '#value' => t('Pay'),
  );

  //Merge submit handlers and add our handler after
  $form['#submit'] = array_merge($form_infos['submit'], array('subs_payment_payment_form_submit'));

  return $form;
}

/**
 * Function submit handler
 *
 * @param $subs
 *   An array keyed by subs id and containing
 *   subs previously created
 *   before calling this submit handler
 */
function subs_payment_payment_form_submit(&$form, &$form_state, $subs = array()) {

  //Start transaction, including others exceptions throw
  $trx = db_transaction();
  try {

    //Retrieve payment and add context data to payment
    //before it will be processed by payment method
    $payment = (isset($form_state['payment'])) ? $form_state['payment'] : NULL;
    if (!$payment) {
      throw new Exception(t('Unable to retrieve payment object for adding metadata.'));
    }
    //Only new payment could be executed
    elseif (!payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_NEW)) {
      throw new Exception(t('Payment (@pid) has its last status "@status" but only payment with status "new" or its ancestor could be executed.', array('@pid' => $payment->pid, '@status' => $payment->getStatus()->status)));
    }

    //Deals with data storage
    if (empty($payment->context)) {
      $payment->context = '';
    }

    //Store subs type label and ids
    $labels = array();
    foreach ($form_state['subs_types_selected'] as $subs_type_id => $subs_type) {
        $labels[] = $subs_type->label;
    }
    //Set context as concatenated labels sub types
    $payment->context .= implode(' | ', $labels);
    //Store subs type ids
    $payment->context_data['subs_type_ids'] = array_keys($form_state['subs_types_selected']);

    //Store subs ids previously created if any provided
    $payment->context_data['subs_ids'] = array_keys($subs);

    //Update line items to store corresponding each associated subs created
    subs_payment_payment_update_line_items($payment, $subs);

    //Save payment before execute it to keep trace on it
    entity_save('payment', $payment);

    //We cannot just execute payment here.
    //Regarding to W3 specs, we could'nt make external redirect
    //(if method is offsite) while a submitted post form is been processing.
    //So we need to execute the payment method in an another place...
    //By the way, we are sure that all submit handler have been executed...

    //Don't rebuild form to redirect it
    $form_state['subs_payment_payment_succeed'] = TRUE;
    $form_state['rebuild'] = FALSE;
    $form_state['redirect'] = SUBS_PAYMENT_PAYMENT_EXECUTE_CALLBACK . '/' . $payment->pid;
  }
  //Unexpected errors, unable to retrieve payment or not in new status ?
  catch (Exception $e) {

    //Revert payment entity saved and line items if any.
    $trx->rollback();

    //Retrieve error message from system if any
    $msg = $e->getMessage();
    if ($msg) {
      watchdog('subs_payment_payment', $msg, NULL, WATCHDOG_ERROR);
    }
    //Weird behavior ?
    else {
      watchdog('subs_payment_payment', 'Error occured during payment transaction
        but cannot find backtrace on it.', array(), WATCHDOG_ERROR);
    }
  }
}