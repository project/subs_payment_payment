<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 * callbacks
 */

/**
 * Let's other context module deals with their own finish callback.
 * This hook is invoke when subs payment payment have finish
 * to execute its own finish callback.
 *
 * @param $payment
 *   A payment object whixch have been executed
 */
function hook_subs_payment_payment_finish_alter(Payment $payment) {
  if (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_SUCCESS)) {
    //deals with payment succeed
  }
  elseif (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_PENDING)) {
    //deals with payment pending
  }
  elseif (payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_FAILED)) {
    //deals with payment failed
  }
}


/**
 * This hook is invoke in cron queue when an expired pending payment
 * was set to fail status.
 * It provide subscriptions associated to this payment and let's context
 * module do appropriate action on these subs.
 *
 * @param $subscriptions
 *   An array keyed by sid and containing subs associated to the payment
 *   changed to fail status.
 */
function hook_subs_payment_payment_cron_payment_pending_cancel($subscriptions) {
  //cancel subs too for example
  foreach ($subscriptions as $sid => $subs) {
    subs_set_cancelled($subs);
  }
}

/**
 * This hook is invoke in cron queue when an expired pending payment
 * was deleted.
 * It provide subscriptions associated to this payment and let's context
 * module do appropriate action on these subs.
 *
 * @param $subscriptions
 *   An array keyed by sid and containing subs associated to the payment deleted
 */
function hook_subs_payment_payment_cron_payment_pending_delete($subscriptions) {
  //delete subs too for example
  subs_delete_multiple(array_keys($subscriptions));
}