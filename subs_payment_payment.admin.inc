<?php

function subs_payment_payment_settings_form($form, &$form_state) {
  $form = array();

  $form['subs_payment_payment_currency_code'] = array(
    '#type' => 'select',
    '#title' => t('Currency code for all subs type'),
    '#options' => payment_currency_options(),
    '#default_value' => variable_get('subs_payment_payment_currency_code', ''),
    '#required' => TRUE,
  );

  $default_rate = variable_get('subs_payment_payment_default_tax_rate', '');
  if ($default_rate) {
    $default_rate *= 100;
  }

  $form['subs_payment_payment_default_tax_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Default tax rate for all subs type'),
    '#description' => t('Set default value while creating a new subs type. Separator must be a dot.'),
    '#size' => 5,
    '#field_suffix' => '%',
    '#default_value' => $default_rate,
    '#element_validate' => array('subs_payment_payment_tax_rate_validate'),
  );

  $terms_conditions = variable_get('subs_payment_payment_default_terms_conditions', FALSE);
  $form['subs_payment_payment_default_terms_conditions'] = array(
    '#type' => 'text_format',
    '#title' => t('Terms and condition'),
    '#description' => t('Explains terms and conditions to the end user through subs payment form. If not empty, it will add a required checkbox to the end user for agreement.'),
    '#default_value' => (!empty($terms_conditions['value'])) ? $terms_conditions['value'] : '',
    '#format' => NULL,
    '#base_type' => 'textarea',
  );

  //Cron job
  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron job'),
    '#description' => t('Some cron jobs executed by the module. For the moment, it\'s only concern offsite payment method.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['cron']['subs_payment_payment_cron_expiration_pending_payment'] = array(
    '#type' => 'textfield',
    '#title' => t('Pending payment lifetime.'),
    '#description' => t('Define how many time (in minutes) a pending payment is valid. IMPORTANT : this setting should be egal or greater than the session gateway lifetime. In case of further offsite payment method available, you should set the greater lifetime of them.'),
    '#default_value' => (variable_get('subs_payment_payment_cron_expiration_pending_payment', SUBS_PAYMENT_PAYMENT_CRON_DEFAULT_EXPIRATION_TIME)) / 60,
    '#element_validate' => array('subs_payment_payment_expiration_validate'),
  );

  $form['cron']['subs_payment_payment_cron_pending_action'] = array(
    '#type' => 'select',
    '#title' => t('Cron job action on payment pending'),
    '#description' => t('With payment method offsite, end-user could do nothing : no cancel, no complete process. Most of the time, session gateway is expired and payment in pending status must be changed, as its context (subs). A cron job will deals with this expired pending payment by cancel or delete it. Note that context module should do some actions on subs associated to the payment (as subs_checkout for example).'),
    '#options' => array(
      SUBS_PAYMENT_PAYMENT_CRON_ACTION_CANCEL => t('Cancel payment'),
      SUBS_PAYMENT_PAYMENT_CRON_ACTION_DELETE => t('Delete payment'),
    ),
    '#default_value' => variable_get('subs_payment_payment_cron_pending_action', SUBS_PAYMENT_PAYMENT_CRON_ACTION_CANCEL),
  );

  return system_settings_form($form);
}

/**
 * Element validate callback
 *
 * @see subs_payment_payment_settings_form()
 */
function subs_payment_payment_expiration_validate($element, &$form_state, $form) {
  if (!empty($element['#value'])) {
    if (!is_numeric($element['#value'])) {
      form_error($element, t('Lifetime pending payment should be a numeric value.'));
    }
    else {
      //convert into secondes for timestamp
      $form_state['values'][$element['#name']] = $element['#value'] * 60;
    }
  }
}
