<?php

function subs_payment_payment_views_data() {
  $data = array();

  $data['subs_payment_payment']['table']['group']  = t('Subs payment payment');

  //Join payment method table
  $data['subs_payment_payment']['table']['join']['subs'] = array(
    'left_field' => 'sid',
    'field' => 'sid',
  );

  //Join payment table
  $data['subs_payment_payment']['table']['join']['payment'] = array(
    'left_field' => 'pid',
    'field' => 'pid',
  );

  //Defines fields
  $data['subs_payment_payment']['sid']['relationship'] = array(
    'title' => t('Subs'),
    'label' => t('subs'),
    'help' => t('Subs associated to the payment'),
    'handler' => 'views_handler_relationship',
    'base' => 'subs',
    'field' => 'sid',
  );

  $data['subs_payment_payment']['pid']['relationship'] = array(
    'title' => t('Payment'),
    'label' => t('payment'),
    'help' => t('Payment associated to the subs'),
    'handler' => 'views_handler_relationship',
    'base' => 'payment',
    'field' => 'pid',
  );

  return $data;
}

/**
 * Implements hook_views_data_alter()
 */
function subs_payment_payment_views_data_alter(&$data) {

  $data['subs_type']['currency_code'] = array(
    'title' => t('Currency'),
    'help' => t('Currency'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['subs_type']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('Amount'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'float' => TRUE,
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['subs_type']['quantity'] = array(
    'title' => t('Quantity'),
    'help' => t('Quantity'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['subs_type']['tax_rate'] = array(
    'title' => t('Tax rate'),
    'help' => t('Tax rate'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'float' => TRUE,
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
}