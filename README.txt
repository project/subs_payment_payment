DESCRIPTION
-----------

This module aims at being a bridge
between subs module (http://drupal.org/project/subs) and payment module
(http://drupal.org/project/payment).

This is a module helper for developer who are looking
for integrate a subs payment system.

It alter subs type entities
to add further payment fields needed. Such as
currency code, amount, tax rate, quantity.

The idea is that each subs will used the
payment property of the subs type attached.

It provide an embeddable payment form to use
in a custom form for selling subs by providing
subs types as a "model" (amount, tax rate, and so on).

It integrate views by providing relationships
between subs and payments.

This module is integrate by the subs checkout module
(http://drupal.org/project/subs_checkout)

-----------
REQUIREMENT
-----------

- subs (http://drupal.org/project/subs) : >= 1.0-beta5
- payment (http://drupal.org/project/payment) : >= 1.5
- currency_api 1.1 OR currency 2.0 (http://drupal.org/project/currency)

-------
INSTALL
-------

- Enable the module like other contrib module.

- Be sure to have at least one payment
  method enable. You can use paymentmethodbasic
  for testing.

- Go to admin/config/workflow/subs/payment_payment
  to set some settings :

-> default currency code to apply for new subs types
-> currency code to use in case of multiple subs type selection (line items)
-> default tax rate to apply
-> custom terms and condition. If not empty,
   add a checkbox required for agreement

- Go to admin/people/permissions and define permissions
  for allowing roles to configure settings

- Create new subs types and complete payment fields

- If subs_checkout is enable, it will add a new checkout pages
  to proceed for payment
  Otherwise, you will need to implements it.

DEVELOPER
---------

PAYMENT FORM

After checking if payment method exists and are enabled, you could
merge payment form in a custom form by simply calling function form id.

Of course, you could merge it manually
<code>
$form += subs_payment_payment_form(array(), $form_state,
$subs_types, $pmids);
</code>

or directly (not recommanded) but be careful about
validate and submit handlers merged
<code>
$form = subs_payment_payment_form($form, $form_state,
$subs_types, $pmids);
</code>

where : 

- $form : a structured form elements to merge directly

- $form_state : must be the caller $form_state

- $subs_types which is an array of subs types entities. Each subs type
entities will be a line item wrap into an order (payment object).
  IMPORTANT : It must be keyed by subs type id.

- $pmids (optional): an array of payment method ID's to restrict

Depending on what you are looking for, you may need
to execute subs_payment_payment_form_submit() manually, passing
subs entities created if any as third arguments (for e.g
by retrieve and merge subs payment payment form rather than
using it directly through drupal_get_form()).

Otherwise, default submit handler will be executed (retrieve
with drupal_get_form for e.g). But in this case, no subs will be tracked.

Execute the payment will be done outside the submit form handler. It will
redirect the form to a function callback to execute it properly by altering
$form_state. In that way, all submit handlers won't be skipped in case of
payment method offsite.
Mostly, doing an http redirect request while a form submitted with POST method
will send a 307 code status. Which should be catch by browser and had to prevent
the end-user. For the moment, only firefox 3.5+ do it correctly with a prompt,
regarding to W3. See http://foswiki.org/Tasks/Item10133 for more details.

After a payment have been executed, it will call default finish callback
implemented by subs payment payment module. After the finish callback have been
executed, it will invoke a hook_alter (hook_subs_payment_payment_finish_alter)
to let's a chance to other context module to deals with the payment executed.

If subs have been passed, a mapping between payment id and
subs ids will be store in database.

It is integrate by subs_checkout module to proceed
for payment by selecting subs types. It could be a kind of
implementation example for developper.

API
---

see subs_payment_payment.api.php for details.

CRON JOBS
---------

A default cron run to track pending payment which haven't
been cancel or complete based on a lifetime session settings. (this settings
should be egal or greater than the remote session payment gateway used).
Matched payment will be cancel (fail status) or delete, depending
on action settings selected.
Then it will call context module to do some actions on subs associated
to the payments founded.